#pragma once

namespace MyApp
{
    void RenderUI();
}

//=============global variables=============

//customer details
static char profession[128] = "";
static char homeAddress[128] = "";
static char streetNumber[128] = "";
static char zipCode[128] = "";
static char townOfResidence[128] = "";
static char phoneNumber[128] = "";
static char landlineNumber[128] = "";
static char email[128] = "";


//last registered day
static char lastRegisteredDay[5];
static char lastRegisteredMonth[5];
static char lastRegisteredYear[5];
static char lastRegisteredHour[5];
static char lastRegisteredMinute[5];


//Latest SNs
static char lastSNChar[64];
static char lastMemberSNChar[64];
static char lastReceiptSNChar[64];

static int lastSNint=0;
static int lastMemberSNint=0;
static int lastReceiptSNint=0;

//current SNs
static char currentSNChar[64];
static char currentMemberSNChar[64];
static char currentReceiptSNChar[64];


//Buttons
static int buttonClear = 0;
