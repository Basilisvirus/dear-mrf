Application to register new members, search the saved members and print the corresponding bills.
written using Dear ImGui: https://github.com/ocornut/imgui
Using Visual studio 2022. 

-You will need to install vulkan to compile this (https://vulkan.lunarg.com/sdk/home#windows) download the sdk installer, but I will switch to opengl in the future. It also uses glfw3.lib, which you can find in the Imgui sub directories there.

-Code uses ClearSans-Regular.ttf and members.json in folder dear-mrf to run. Make sure members.json file does not contain "bad" .json data. If unsure, default the file to { }  
![code folder](https://i.stack.imgur.com/0r3Us.png)  
-Code exports test.pdf when asked to print/export pdf.  
-The compiled x64 files are in dear-mrf\x64\debug or dear-mrf\x64\release, depending on the compilation option from visual studio.  
-You need to have some files in the same directory as the project/compiled file to run the code/.exe sucesfully. These are ClearSans-Regular.ttf for the fonts, members.json for the list of members, zlib.dll, libpng16.dll, hpdf.dll. Release folder:  
![dear-mrf\x64\Release](https://i.stack.imgur.com/fhN1S.png)  
Debug folder:  
![dear-mrf\x64\Debug](https://i.stack.imgur.com/TWRdr.png)  
You can find the above mentioned files in libharu_12_10_2022_github, pnglib and Zlib1_2_13 directories, in the "Compiled files" sub-directories. I have compiled them my own.  
-Opening the project, you will need to include all these imgui files shown (right click on the project/add/existing and add these:  
![Include files](https://i.stack.imgur.com/XzPzw.png)  
The above ImGui files are found in the folders of the project.  
-Now to compile, you will need to change the included files, since the directories I use are absolute and not relative directories. Check my settings:  
![General properties](https://i.stack.imgur.com/UuRuP.png)  
![Directories 1](https://i.stack.imgur.com/Op8MD.png)  
![Directories 2](https://i.stack.imgur.com/MRUik.png)  
![Directories 2](https://i.stack.imgur.com/MRUik.png)  
I dont use anything in C/C++ external includes.  
Command line:  
![command line](https://i.stack.imgur.com/JStXf.png)  
Linker input your .libs:  
![Linker](https://i.stack.imgur.com/7Kod9.png)  
Linker command line:  
![Linker command line](https://i.stack.imgur.com/v8vv3.png)  
Also check on my paths here, something might be missing for you, vulkan path for example:  
![Path](https://i.stack.imgur.com/Oc8pn.png)  