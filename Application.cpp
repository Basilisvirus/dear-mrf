﻿#include "Application.h"
#include "imgui.h"

#include <iostream> //for uint_8t
#include <fstream> //https://github.com/nlohmann/json#read-json-from-a-file
#include <iomanip> //Readme: https://github.com/nlohmann/json/tree/develop/docs/examples

#include <json.hpp>//for json

#include <stdlib.h>//for libharu
#include <stdio.h>//for libharu
#include <string.h>//for libharu
#include <math.h>
#include <setjmp.h>//for libharu

#include "hpdf.h"//for libharu

#include <assert.h>

#include <windows.h> // use to define SYSTEMTIME , GetLocalTime() and GetSystemTime()



#define local static

//libharu START
jmp_buf env;


#ifdef HPDF_DLL
void  __stdcall
#else
void
#endif
error_handler(HPDF_STATUS   error_no,
    HPDF_STATUS   detail_no,
    void* user_data)
{
    printf("ERROR: error_no=%04X, detail_no=%u\n", (HPDF_UINT)error_no,
        (HPDF_UINT)detail_no);
    longjmp(env, 1);
}

const char* font_list[] = {
    "Courier",
    "Courier-Bold",
    "Courier-Oblique",
    "Courier-BoldOblique",
    "Helvetica",
    "Helvetica-Bold",
    "Helvetica-Oblique",
    "Helvetica-BoldOblique",
    "Times-Roman",
    "Times-Bold",
    "Times-Italic",
    "Times-BoldItalic",
    "Symbol",
    "ZapfDingbats",
    NULL
};
//libharu END

//https://github.com/nlohmann/json
using json = nlohmann::json;



/*
When search is being done, the return strings contain quotation marks
at the beggining and at the end of each string.
I remove these quotationmarks here.
*/
void removeQuotationMarks(char* arrayToEdit)//input is a char array
{
    uint16_t sizeLoop = strlen(arrayToEdit);//get the size of the containing characters
    uint16_t loopFor;

    for (loopFor = 0; loopFor <= sizeLoop; loopFor++)//move all chars one position before to get rid of first quotation
    {
        arrayToEdit[loopFor] = arrayToEdit[loopFor + 1];
    }
    sizeLoop = strlen(arrayToEdit);//get the new array, it should be smaller by 1

    arrayToEdit[sizeLoop - 1] = NULL;//NULL at the last quotation mark
}

//find the last SN,memberSN,receiptSN and keep them in memory.
//when a new member will be registered, the SN will increase automatically by 1.
void checkLastSN()
{
    std::cout << "in checkLastSN()\n";
    //open file to take the contents.
    std::fstream input_file;
    input_file.open("members.json");
    std::cout << "parsing.. (make sure members.json file is not in bad form)\n";
    json membersJson = json::parse(input_file);
    input_file.close();//close the file, proceed with the editing of the file.

    json::iterator it, it2;
    std::string membersValuesString;//save the key string of each iteration here.

    //iterate through AMKAs
    for (it = membersJson.begin(); it != membersJson.end(); ++it)
    {
        //membersJsonString = it.value().dump();

        //std::cout << "it.key(): " << it.key() << " ";
        //std::cout << "it.value(): " << it.value() << "\n";

        json memberValuesJson = it.value();

        //for each AMKA iterate through its values
        for (it2 = memberValuesJson.begin(); it2 != memberValuesJson.end(); ++it2)
        {
            //std::cout << "it2.key(): " << it2.key() << " ";
            //std::cout << "it2.value(): " << it2.value() << "\n";

            static uint64_t lastSNTemp;

            if (it2.key() == "SN")
            {   
                membersValuesString = it2.value();
                strncpy(lastSNChar, membersValuesString.c_str(), membersValuesString.size());
                lastSNTemp = strtol(lastSNChar,0,10);//convert from char array to int

                //if this SN is larger than the lastSNint then replace it.
                if (lastSNTemp > lastSNint)
                {
                    lastSNint = lastSNTemp;
                }
            }
            else if (it2.key() == "memberSN")
            {
                membersValuesString = it2.value();
                strncpy(lastMemberSNChar, membersValuesString.c_str(), membersValuesString.size());
                lastSNTemp = strtol(lastMemberSNChar, 0, 10);//convert from char array to int

                //if this SN is larger than the lastSNint then replace it.
                if (lastSNTemp > lastMemberSNint)
                {
                    lastMemberSNint = lastSNTemp;
                }
            }
            else if (it2.key() == "receiptSN")
            {
                membersValuesString = it2.value();
                strncpy(lastReceiptSNChar, membersValuesString.c_str(), membersValuesString.size());
                lastSNTemp = strtol(lastReceiptSNChar, 0, 10);//convert from char array to int

                //if this SN is larger than the lastSNint then replace it.
                if (lastSNTemp > lastReceiptSNint)
                {
                    lastReceiptSNint = lastSNTemp;
                }
            }
        }

    }
    std::cout << "lastSNint: " << lastSNint << "\n";
    std::cout << "lastMemberSNint: " << lastMemberSNint << "\n";
    std::cout << "lastReceiptSNint: " << lastReceiptSNint << "\n";

    //also replace char arrays with the lastest SNs
    itoa(lastSNint, lastSNChar, 10);//convert from int to char and save it on the array
    itoa(lastMemberSNint, lastMemberSNChar, 10);//convert from int to char and save it on the array
    itoa(lastReceiptSNint, lastReceiptSNChar, 10);//convert from int to char and save it on the array
}


namespace MyApp
{
    void RenderUI()
    {
        static uint8_t repeat = 0;

        if (repeat == 0)
        {
            std::cout << "run once\n\r";
            repeat = 1;
            SetConsoleOutputCP(CP_UTF8);//Install and launch Windows Terminal https://stackoverflow.com/a/65064849/13294095
            checkLastSN();
        }//==============end run once=================

        static bool opt_fullscreen = true;
        static bool opt_padding = false;
        static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

        // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
        // because it would be confusing to have two docking targets within each others.
        ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
        if (opt_fullscreen)
        {
            const ImGuiViewport* viewport = ImGui::GetMainViewport();
            ImGui::SetNextWindowPos(viewport->WorkPos);
            ImGui::SetNextWindowSize(viewport->WorkSize);
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
            window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
        }
        else
        {
            dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
        }

        // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background
        // and handle the pass-thru hole, so we ask Begin() to not render a background.
        if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
            window_flags |= ImGuiWindowFlags_NoBackground;

        // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
        // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
        // all active windows docked into it will lose their parent and become undocked.
        // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
        // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
        if (!opt_padding)
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        ImGui::Begin("DockSpace Demo", nullptr, window_flags);
        if (!opt_padding)
            ImGui::PopStyleVar();

        if (opt_fullscreen)
            ImGui::PopStyleVar(2);

        // Submit the DockSpace
        ImGuiIO& io = ImGui::GetIO();
        if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
        {
            ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
            ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
        }

        if (ImGui::BeginMenuBar())
        {
            if (ImGui::BeginMenu("Options"))
            {
                // Disabling fullscreen would allow the window to be moved to the front of other windows,
                // which we can't undo at the moment without finer window depth/z control.
                ImGui::MenuItem("Fullscreen", NULL, &opt_fullscreen);
                ImGui::MenuItem("Padding", NULL, &opt_padding);
                ImGui::Separator();

                if (ImGui::MenuItem("Flag: NoSplit", "", (dockspace_flags & ImGuiDockNodeFlags_NoSplit) != 0)) { dockspace_flags ^= ImGuiDockNodeFlags_NoSplit; }
                if (ImGui::MenuItem("Flag: NoResize", "", (dockspace_flags & ImGuiDockNodeFlags_NoResize) != 0)) { dockspace_flags ^= ImGuiDockNodeFlags_NoResize; }
                if (ImGui::MenuItem("Flag: NoDockingInCentralNode", "", (dockspace_flags & ImGuiDockNodeFlags_NoDockingInCentralNode) != 0)) { dockspace_flags ^= ImGuiDockNodeFlags_NoDockingInCentralNode; }
                if (ImGui::MenuItem("Flag: AutoHideTabBar", "", (dockspace_flags & ImGuiDockNodeFlags_AutoHideTabBar) != 0)) { dockspace_flags ^= ImGuiDockNodeFlags_AutoHideTabBar; }
                if (ImGui::MenuItem("Flag: PassthruCentralNode", "", (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode) != 0, opt_fullscreen)) { dockspace_flags ^= ImGuiDockNodeFlags_PassthruCentralNode; }
                ImGui::Separator();

                ImGui::EndMenu();
            }
            ImGui::EndMenuBar();
        }

        //#pragma execution_character_set("utf-8") //https://support.microsoft.com/en-us/topic/fix-string-literals-are-misinterpreted-by-the-visual-c-compiler-in-visual-studio-2010-when-the-string-literals-use-utf-8-encoding-and-contain-asian-characters-84f3881b-99bc-95a8-c774-35c187c59c13

        ImGui::Begin(u8"Εγγραφή ανανέωσης μέλους");

        ImGui::Text(u8"Προσωπικά στοιχεία");
        
        static char name[128] = "";
        ImGui::InputTextWithHint(u8"Όνομα", u8"Όνομα", name, IM_ARRAYSIZE(name));

        static char surname[128] = "";
        ImGui::InputTextWithHint(u8"Επίθετο", u8"Επίθετο", surname, IM_ARRAYSIZE(surname));

        static char fathersName[128] = "";
        ImGui::InputTextWithHint(u8"Όνομα πατρός", u8"Όνομα πατρός", fathersName, IM_ARRAYSIZE(fathersName));

        static char birthDate[128] = "";
        ImGui::InputTextWithHint(u8"Ημερομηνία γέννησης DD/MM/YYYY", u8"Ημερομηνία γέννησης DD/MM/YYYY", birthDate, IM_ARRAYSIZE(birthDate));

        static char IDNumber[128] = "";
        ImGui::InputTextWithHint(u8"Αριθμός ταυτότητας", u8"Αριθμός ταυτότητας", IDNumber, IM_ARRAYSIZE(IDNumber));

        static char PDIssued[128] = "";
        ImGui::InputTextWithHint(u8"ΑΤ έκδοσης", u8"ΑΤ έκδοσης", PDIssued, IM_ARRAYSIZE(PDIssued));

        static char AMKA[128] = "";
        ImGui::InputTextWithHint(u8"ΑΜΚΑ", u8"ΑΜΚΑ", AMKA, IM_ARRAYSIZE(AMKA));
        ImGui::SameLine();

        static uint8_t buttonSearch = 0;
        static uint8_t buttonSearchTimer = 0;
        if (ImGui::Button(u8"Αναζήτηση"))
        {
            buttonSearch = 1;//button search pressed.
            ImGui::Text(u8"Αναζήτηση...");

            //open file to take the contents.
            std::fstream input_file;
            input_file.open("members.json");
            json membersJson = json::parse(input_file);
            input_file.close();//close the file, proceed with the editing of the file.

            // find an entry
            if (membersJson.contains(AMKA))
            {
                std::cout << "it contains the desired AMKA\n\r";
                ImGui::SameLine();
                ImGui::Text(u8"Βρέθηκε");

                if (membersJson.find(AMKA) != membersJson.end() )
                {
                    std::cout << "The desired AMKA was found\n\r";
                    json::iterator membersIterator = membersJson.find(AMKA);//returns the iterator to this AMKA


                    std::cout << "\"AMKA\" was found: " << (membersIterator != membersJson.end()) << '\n';
                    //std::cout << "value at key \"AMKA\": \n\r" << *membersIterator << '\n';

                    /* 
                    https://github.com/nlohmann/json/discussions/2243 Iterating over list of objects
                    https://github.com/nlohmann/json/discussions/3741 How to convert the values of the json to string or char array
                    */

                    /*
                    //iterator way 1:
                    std::string key, value;
                    for (auto& [key, value] : membersIterator->items())
                    {
                        std::cout << "key: " << key << " Value: " << value << "\n";
                    }
                    */
                    json::iterator it;//make the iterator
                    std::string jsonValueString;//save the key string of each iteration here.
                    // special iterator member functions for objects
                    for (it = membersIterator->begin(); it != membersIterator->end(); ++it)
                    {
                        jsonValueString = it.value().dump(); //it.value().get_ref<json::string_t&>();//save the key string

                        if (it.key() == "lastRegisterDate")
                        {
                            json registeredDate = it.value();//json::parse( R"({"Day":"6", "Hour" : "15", "Minute" : "7", "Month" : "11", "Year" : "2022"})");

                            json::iterator regDateIt;
                            std::string regDateString;//save the key string of each iteration here.

                            for (regDateIt = registeredDate.begin(); regDateIt != registeredDate.end(); ++regDateIt)
                            {
                                regDateString = regDateIt.value();

                                if (regDateIt.key() == "Day")
                                {
                                    strncpy(lastRegisteredDay, regDateString.c_str(), regDateString.size());
                                }
                                else if (regDateIt.key() == "Hour")
                                {
                                    strncpy(lastRegisteredHour, regDateString.c_str(), regDateString.size());
                                }
                                else if (regDateIt.key() == "Minute")
                                {
                                    strncpy(lastRegisteredMinute, regDateString.c_str(), regDateString.size());
                                }
                                else if (regDateIt.key() == "Month")
                                {
                                    strncpy(lastRegisteredMonth, regDateString.c_str(), regDateString.size());
                                }
                                else if (regDateIt.key() == "Year")
                                {
                                    strncpy(lastRegisteredYear, regDateString.c_str(), regDateString.size());
                                }
                                
                            }

                            /*
                            for (itReg = registeredDateIterator->begin(); itReg != registeredDateIterator->end(); ++itReg)
                            {
                                std::cout << "value= " << itReg.value() << "\n";
                            }
                            */

                        }


                        if (it.key() == "IDNumber")//IDNumber
                        {
                            strncpy(IDNumber, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(IDNumber);
                            IDNumber[strlen(IDNumber)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(IDNumber); loopFor++)
                            {
                                IDNumber[loopFor] = '\0';
                            }

                        }
                        else if (it.key() == "PDIssued")//PDIssued
                        {
                            strncpy(PDIssued, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(PDIssued);
                            PDIssued[strlen(PDIssued)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(PDIssued); loopFor++)
                            {
                                PDIssued[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "birthDate")//birthDate
                        {
                            strncpy(birthDate, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(birthDate);
                            birthDate[strlen(birthDate)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(birthDate); loopFor++)
                            {
                                birthDate[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "email")//email
                        {
                            strncpy(email, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(email);
                            email[strlen(email)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(email); loopFor++)
                            {
                                email[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "fathersName")//fathersName
                        {
                            strncpy(fathersName, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(fathersName);
                            fathersName[strlen(fathersName)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(fathersName); loopFor++)
                            {
                                fathersName[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "homeAddress")//homeAddress
                        {
                            strncpy(homeAddress, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(homeAddress);
                            homeAddress[strlen(homeAddress)] = '\0';

                            for (int loopFor = (jsonValueString.size()-2); loopFor < sizeof(homeAddress); loopFor++)
                            {
                                homeAddress[loopFor] = '\0';
                            }

                        }
                        else if (it.key() == "landlineNumber")//landlineNumber
                        {
                            strncpy(landlineNumber, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(landlineNumber);
                            landlineNumber[strlen(landlineNumber)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(landlineNumber); loopFor++)
                            {
                                landlineNumber[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "name")//name
                        {
                            strncpy(name, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(name);
                            name[strlen(name)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(name); loopFor++)
                            {
                                name[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "phoneNumber")//phoneNumber
                        {
                            strncpy(phoneNumber, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(phoneNumber);
                            phoneNumber[strlen(phoneNumber)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(phoneNumber); loopFor++)
                            {
                                phoneNumber[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "profession")//profession
                        {
                            strncpy(profession, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(profession);
                            profession[strlen(profession)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(profession); loopFor++)
                            {
                                profession[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "streetNumber")//streetNumber
                        {
                            strncpy(streetNumber, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(streetNumber);
                            streetNumber[strlen(streetNumber)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(streetNumber); loopFor++)
                            {
                                streetNumber[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "surname")//surname
                        {
                            strncpy(surname, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(surname);
                            surname[strlen(surname)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(surname); loopFor++)
                            {
                                surname[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "townOfResidence")//townOfResidence
                        {
                            strncpy(townOfResidence, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(townOfResidence);
                            townOfResidence[strlen(townOfResidence)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(townOfResidence); loopFor++)
                            {
                                townOfResidence[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "zipCode")//zipCode
                        {
                            strncpy(zipCode, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(zipCode);
                            zipCode[strlen(zipCode)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(zipCode); loopFor++)
                            {
                                zipCode[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "SN")//SN
                        {
                            strncpy(currentSNChar, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(currentSNChar);
                            currentSNChar[strlen(currentSNChar)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(currentSNChar); loopFor++)
                            {
                                currentSNChar[loopFor] = '\0';
                            }
                        }
                        else if (it.key() == "memberSN")//SN
                        {
                            strncpy(currentMemberSNChar, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(currentMemberSNChar);
                            currentMemberSNChar[strlen(currentMemberSNChar)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(currentMemberSNChar); loopFor++)
                            {
                                currentMemberSNChar[loopFor] = '\0';
                            }

                        }
                        else if (it.key() == "receiptSN")//SN
                        {
                            strncpy(currentReceiptSNChar, jsonValueString.c_str(), jsonValueString.size());
                            removeQuotationMarks(currentReceiptSNChar);
                            currentReceiptSNChar[strlen(currentReceiptSNChar)] = '\0';

                            for (int loopFor = (jsonValueString.size() - 2); loopFor < sizeof(currentReceiptSNChar); loopFor++)
                            {
                                currentReceiptSNChar[loopFor] = '\0';
                            }
                        }
                    }
                }

            }
            else
            {

                std::cout << "AMKA" << AMKA <<"Does not exist\n\r";
                ImGui::SameLine();
                ImGui::Text(u8"Δε βρέθηκε");
                buttonClear = 1;
                buttonSearch = 0;
            }
        }

        
        ImGui::InputTextWithHint(u8"Επάγγελμα", u8"Επάγγελμα", profession, IM_ARRAYSIZE(profession));

        ImGui::Text(u8"Διεύθυνση κατοικίας");

        ImGui::InputTextWithHint(u8"Διέυθυνση", u8"Διέυθυνση", homeAddress, IM_ARRAYSIZE(homeAddress));

        ImGui::InputTextWithHint(u8"Αριθμός οδού", u8"Αριθμός οδού", streetNumber, IM_ARRAYSIZE(streetNumber));

        ImGui::InputTextWithHint(u8"ΤΚ", u8"ΤΚ", zipCode, IM_ARRAYSIZE(zipCode));

        ImGui::InputTextWithHint(u8"Πόλη", u8"Πόλη", townOfResidence, IM_ARRAYSIZE(townOfResidence));

        ImGui::Text(u8"Στοιχεία επικοινωνίας");

        
        ImGui::InputTextWithHint(u8"Αριθμός κινητού τηλεφώνου", u8"Αριθμός κινητού τηλεφώνου", phoneNumber, IM_ARRAYSIZE(phoneNumber));

        ImGui::InputTextWithHint(u8"Αριθμός σταθερού τηλεφώνου", u8"Αριθμός σταθερού τηλεφώνου", landlineNumber, IM_ARRAYSIZE(landlineNumber));

        ImGui::InputTextWithHint("email", "email", email, IM_ARRAYSIZE(email));

        //if button search is pressed, show the last registered day
        if (buttonSearch)
        {
            ImGui::Text(u8"Τελευταία Εγγραφή: ");
            ImGui::SameLine();
            ImGui::Text(lastRegisteredDay);
            ImGui::SameLine();
            ImGui::Text("/");
            ImGui::SameLine();
            ImGui::Text(lastRegisteredMonth);
            ImGui::SameLine();
            ImGui::Text("/");
            ImGui::SameLine();
            ImGui::Text(lastRegisteredYear);
            ImGui::SameLine();
            ImGui::Text(u8"στις");
            ImGui::SameLine();
            ImGui::Text(lastRegisteredHour);
            ImGui::SameLine();
            ImGui::Text(":");
            ImGui::SameLine();
            ImGui::Text(lastRegisteredMinute);

            ImGui::Text(u8"Α/Α: ");
            ImGui::SameLine();
            ImGui::Text(currentSNChar);

            ImGui::Text(u8"Α/Α μέλους: ");
            ImGui::SameLine();
            ImGui::Text(currentMemberSNChar);

            ImGui::Text(u8"Α/Α απόδειξης: ");
            ImGui::SameLine();
            ImGui::Text(currentReceiptSNChar);

        }

        static uint8_t buttonRegisterTimer = 0;
        static uint8_t buttonRegister = 0;
        if (ImGui::Button(u8"Εγγραφή"))
        {
            buttonRegister = 1;
            buttonRegisterTimer++;

            buttonSearch = 0;//reset button searched
        }

        static uint8_t buttonClearTimer = 0;
        if (ImGui::Button(u8"Εκκαθάριση"))
        {
            buttonClear = 1;
            buttonClearTimer++;

            buttonSearch = 0;
        }

        //register the member to the .json file, based on AMKA.
        if (buttonRegister)
        {
            buttonRegister = 0;

            SYSTEMTIME time; // Declare SYSTEMTIME struct to get current time
            GetLocalTime(&time); // Fill out the struct so that it can be used
            // Use GetSystemTime(&t) to get UTC time 
            //printf("Year: %d, Month: %d, Day: %d, Hour: %d, Minute:%d, Second: %d, Millisecond: %d", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond, time.wMilliseconds); // Return year, month, day, hour, minute, second and millisecond in that order
            static char currentDay[5];
            itoa(time.wDay, currentDay, 10);//convert from int to char and save it on the array
            static char currentMonth[5];
            itoa(time.wMonth, currentMonth, 10);//convert from int to char and save it on the array
            static char currentYear[5];
            itoa(time.wYear, currentYear, 10);//convert from int to char and save it on the array
            static char currentHour[5];
            itoa(time.wHour, currentHour, 10);//convert from int to char and save it on the array
            static char currentMinute[5];
            itoa(time.wMinute, currentMinute, 10);//convert from int to char and save it on the array

            //https://github.com/nlohmann/json/discussions/3737 How to write the edited .json back to the .json file
            
            //Register them to .json file.

            //open file to take the contents.
            std::fstream input_file;
            input_file.open("members.json");



            json membersJson = json::parse(input_file);
            input_file.close();//close the file, proceed with the editing of the file.

            //increase SNs
            lastSNint++;
            lastMemberSNint++;
            lastReceiptSNint++;

            itoa(lastSNint, lastSNChar, 10);//convert from int to char and save it on the array
            itoa(lastMemberSNint, lastMemberSNChar, 10);//convert from int to char and save it on the array
            itoa(lastReceiptSNint, lastReceiptSNChar, 10);//convert from int to char and save it on the array

            //std::cout << "Before: " << membersJson << '\n';
            membersJson[AMKA] =
            {
                {"name", name},
                {"surname", surname},
                {"fathersName", fathersName},
                {"birthDate", birthDate},
                {"IDNumber", IDNumber},
                {"PDIssued",PDIssued},
                {"profession",profession},
                {"homeAddress",homeAddress},
                {"streetNumber", streetNumber},
                {"zipCode",zipCode},
                {"townOfResidence",townOfResidence},
                {"phoneNumber",phoneNumber},
                {"landlineNumber",landlineNumber},
                {"email",email},
                {"SN",lastSNChar},
                {"memberSN",lastMemberSNChar},
                {"receiptSN",lastReceiptSNChar},
                {"lastRegisterDate",
                    {
                        {"Day",currentDay},
                        {"Month",currentMonth},
                        {"Year",currentYear},
                        {"Hour",currentHour},
                        {"Minute",currentMinute}
                    }
                }

            };
            //std::cout << "After: " << membersJson << '\n';

            //edits done, open file again.
            std::fstream output_file;
            output_file.open("members.json");
            std::string output_string = membersJson.dump(4);

            output_file.write(output_string.c_str(), output_string.size());
            output_file.close();
        }

        if (buttonRegisterTimer)
        {
            buttonRegisterTimer++;

            ImGui::SameLine();
            ImGui::Text(u8"Η εγγραφή καταχωρήθηκε");
        }

        //Clear the input values.
        if (buttonClear)
        {
            std::cout << "ButtonClear\n\r";
            buttonClear = 0;

            *AMKA = '\0';
            *name = '\0';
            *surname = '\0';
            *fathersName = '\0';
            *birthDate = '\0';
            *IDNumber = '\0';
            *PDIssued = '\0';
            *profession = '\0';
            *homeAddress = '\0';
            *streetNumber = '\0';
            *zipCode = '\0';
            *townOfResidence = '\0';
            *phoneNumber = '\0';
            *landlineNumber = '\0';
            *email = '\0';
        }


        ImGui::End();

        ImGui::End();//for ImGui::Begin(u8"Εγγραφή ανανέωσης μέλους");

        ImGui::Begin(u8"Πληρωμή");
        if (ImGui::Button(u8"Εξαγωγή PDF"))
        {

            const char* page_title = u8"ΕΙΣΙΤΗΡΙΟ ΘΕΑΜΑΤΟΣ";
            HPDF_Doc  pdf;
            char fname[256];
            HPDF_Page page;
            const char* fontname;//to load my own font
            HPDF_Font def_font;
            HPDF_REAL tw;
            HPDF_REAL height;
            HPDF_REAL width;
            HPDF_UINT i;

            strcpy(fname, "test");
            strcat(fname, ".pdf");

            pdf = HPDF_New(error_handler, NULL);
            if (!pdf) {
                printf("error: cannot create PdfDoc object\n");
                std::cout << "error: cannot create PdfDoc object\n\r";
            }

            if (setjmp(env)) {
                HPDF_Free(pdf);
                std::cout << "pdf freed\n\r";
            }
            HPDF_UseUTFEncodings(pdf);//to use utf encodings
            fontname = HPDF_LoadTTFontFromFile(pdf, "ClearSans-Regular.ttf", HPDF_TRUE);//load my font
            def_font = HPDF_GetFont(pdf, fontname, "UTF-8");//replace default font

            /* Add a new page object. */
            page = HPDF_AddPage(pdf);
            HPDF_Page_SetSize(page, HPDF_PAGE_SIZE_A4, HPDF_PAGE_PORTRAIT);//Set attributes of the page object

            height = HPDF_Page_GetHeight(page);
            width = HPDF_Page_GetWidth(page);

            /* Print the lines of the page. */
            //HPDF_Page_SetLineWidth(page, 1);
            //HPDF_Page_Rectangle(page, 50, 50, width - 100, height - 110);
            //HPDF_Page_Stroke(page);

            /* Print the title of the page (with positioning center). */
            //def_font = HPDF_GetFont(pdf, font_list[12], NULL);
            //HPDF_GetFont(pdf, "Helvetica", NULL);
            HPDF_Page_SetFontAndSize(page, def_font, 12);

            tw = HPDF_Page_TextWidth(page, page_title);
            HPDF_Page_BeginText(page);
            HPDF_Page_TextOut(page, (width - tw) / 2, height - 40, page_title);
            HPDF_Page_EndText(page);

            /* output subtitle. */
            HPDF_Page_BeginText(page);
            HPDF_Page_SetFontAndSize(page, def_font, 8);
            HPDF_Page_TextOut(page, 30, height - 60, u8"Παραγγελία: ......");
            HPDF_Page_TextOut(page, 215, height - 60, u8"ΠΑΝΕΛΛΑΔΙΚΗ ΛΕΣΧΗ ΦΙΛΩΝ ΠΑΝΑΘΗΝΑΪΚΟΥ");
            HPDF_Page_SetFontAndSize(page, def_font, 5);
            HPDF_Page_TextOut(page, 480, height - 55, u8"Λεωφ. Αλεξάνδρας 160, ΑΘΗΝΑ");
            HPDF_Page_TextOut(page, 480, height - 63, u8"ΑΦΜ: 090019647, Δ.Ο.Υ.: ΨΥΧΙΚΟΥ");
            HPDF_Page_SetFontAndSize(page, def_font, 8);
            HPDF_Page_TextOut(page, 30, height - 80, u8"ΗΜ/ΝΙΑ ΠΑΡΑΓΓΕΛΙΑΣ: .......");
            HPDF_Page_TextOut(page, (width - tw) / 2, height - 80, u8"ΑΡ. ΠΑΡΑΣΤΑΤΙΚΟΥ: ........");
            HPDF_Page_TextOut(page, (width - tw) / 2, height - 100, u8"ΗΜΕΡΟΜΗΝΙΑ ΠΑΡΑΣΤΑΤΙΚΟΥ: ........");
            HPDF_Page_EndText(page);

            HPDF_Page_BeginText(page);

            const char* samp_text = u8": ΕΘΝ. ΑΝΤΙΣΤΑΣΕΩΣ 38, ΤΚ: 16121";

            std::cout << "sizeof homeAddress is:" << strlen(homeAddress) << "\n\r";
            std::cout << "sizeof streetnumber is:" << strlen(streetNumber) << "\n\r";

            HPDF_Page_TextOut(page, 30, height - 140, u8"ΔΙΕΥΘΥΝΣΗ: ");
            HPDF_Page_TextOut(page, 105, height - 140, homeAddress);
            uint16_t homeAddressWidth = HPDF_Page_TextWidth(page, homeAddress);
            HPDF_Page_TextOut(page, homeAddressWidth + 108, height - 140, streetNumber);
            HPDF_Page_TextOut(page, 30, height - 155, u8"ΣΥΝΑΛΛΑΣΟΜΕΝΟΣ: ");
            HPDF_Page_TextOut(page, 105, height - 155, name);
            uint16_t nameWidth = HPDF_Page_TextWidth(page, name);
            HPDF_Page_TextOut(page, 108 + nameWidth, height - 155, surname);
            HPDF_Page_TextOut(page, 30, height - 170, u8"EMAIL: ");
            HPDF_Page_TextOut(page, 105, height - 170, email);
            HPDF_Page_TextOut(page, 30, height - 185, u8"ΤΗΛΕΦΩΝΟ: ");
            HPDF_Page_TextOut(page, 105, height - 185, phoneNumber);

            HPDF_Page_TextOut(page, 30, height - 225, u8"ΗΜ/ΝΙΑ ΠΛΗΡΩΜΗΣ: .......");
            HPDF_Page_TextOut(page, 30, height - 240, u8"ΣΥΝΟΛΟ: .....");
            HPDF_Page_TextOut(page, 30, height - 255, u8"ΣΥΝΟΛΟ ΠΡΟ ΦΠΑ: ......");
            HPDF_Page_TextOut(page, 30, height - 270, u8"ΤΡΟΠΟΣ ΠΛΗΡΩΜΗΣ: ....");
            HPDF_Page_TextOut(page, 140, height - 300, u8"ΕΚΔΗΛΩΣΗ");

            HPDF_Page_SetFontAndSize(page, def_font, 7);
           
            HPDF_Page_SetFontAndSize(page, def_font, 6);
            HPDF_Page_TextOut(page, 30, height - 320, u8"Φορ. Υπογραφή: Αθεώρητο βάσει ΠΟΛ-1288/31-12-2013");

            HPDF_Page_SetFontAndSize(page, def_font, 7);
            HPDF_Page_TextOut(page, 30, height - 340, u8"Υπογραφή");
            HPDF_Page_TextOut(page, 30, height - 370, u8"------------------------------");

            HPDF_Page_EndText(page);

            HPDF_SaveToFile(pdf, fname);

            /* clean up */
            HPDF_Free(pdf);

            //ShellExecute(NULL, "open", "test.pdf", NULL, NULL, SW_SHOWNORMAL);

        }

        ImGui::End();//for ImGui::Begin(u8"Πληρωμή");

        //ImGui::ShowDemoWindow();
    }
}
